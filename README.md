# User Microservice

App written in Go

Run the following

## SETUP

1) Install Golang version >= 1.12

```
apt install golang-go
```

2) Verify version is >= 1.12

```
go version
```

3) Add the following to profile file ( the path where go is installed)

```
vi ~/.bash_profile
export PATH=$PATH:/usr/local/go/bin
```

4) Make a go folder inside your home directory. Then run the following commands:

```
cd go
mkdir bin src
cd src
git clone {repo-url}
export GOPATH={path of go directory that you created}
go get
go install
```

5) We are using dep as a dependeny manager for golang. To install dep run:

```
curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

Once Installation is completed, add dep path to your environment variables. To do so, add the following to your .bashrc or ./bash_profile
```
export PATH=$PATH:$GOPATH/bin
```

6) Run the following command to install dependencies (in project root directory)

```
go get
go mod tidy
```

7) To build and install the app (every time changes are made to the code inside the repo):

```
go install
```

8) Run the server using the following command

```
cd $GOPATH/bin
./user_crud
```

This will start the Service on port 8808.
